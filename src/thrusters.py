import rospy
import time
from rospy_tutorials.msg import Floats

rospy.init_node('feedfwd')
pub = rospy.Publisher('/thrusters', Floats, queue_size=10)
rate = rospy.Rate(10)

start = time.time()

while not rospy.is_shutdown() and (time.time() - start) < 2.0:
	if time.time() - start < 2.0:
		thrusters = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	else:
		thrusters = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	thrustermsg = Floats()
	thrustermsg = thrusters
	pub.publish(thrustermsg)
	
	rate.sleep()


#!/usr/bin/env python

import numpy as np
import math
import time
import roslib
import rospy
import tf
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped, Vector3Stamped, TransformStamped, Twist, Wrench, Pose, Point, Quaternion, PointStamped

if __name__=='__main__':
    rospy.init_node('tftesting')
    wrench_pub = rospy.Publisher('/cmd_force', Wrench, queue_size=10)
    rate = rospy.Rate(50)

    while not rospy.is_shutdown():
        tb = tf2_ros.TransformBroadcaster()
        t = TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "world"
        t.child_frame_id = "base_link"
        q = tf.transformations.quaternion_from_euler(0, 0, 0)
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]
        t.transform.translation.x = 0.0
        t.transform.translation.y = 0.0
        t.transform.translation.z = -0.5

        tgoal = TransformStamped()
        tgoal.header.stamp = rospy.Time.now()
        tgoal.header.frame_id = "world"
        tgoal.child_frame_id = "goal"
        qgoal = tf.transformations.quaternion_from_euler(0, 0, math.pi/4)
        tgoal.transform.rotation.x = qgoal[0]
        tgoal.transform.rotation.y = qgoal[1]
        tgoal.transform.rotation.z = qgoal[2]
        tgoal.transform.rotation.w = qgoal[3]
        tgoal.transform.translation.x = 0.0
        tgoal.transform.translation.y = 0.0
        tgoal.transform.translation.z = -0.25

        wrench = Wrench()
        wrench.force.x = 1.0
        wrench.force.y = 0.0
        wrench.force.z = 0.0
        wrench.torque.x = 1.0
        wrench.torque.y = 0.0
        wrench.torque.z = 0.0

        wrench_pub.publish(wrench)
        tb.sendTransform(t)
        tb.sendTransform(tgoal)

        rate.sleep()

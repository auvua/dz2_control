import rospy
import time
from rospy_tutorials.msg import Floats

rospy.init_node('feedfwd')
pub = rospy.Publisher('/pneumatics', Floats, queue_size=10)
rate = rospy.Rate(10)

start = time.time()

while not rospy.is_shutdown():
	if (time.time() - start) < 0.125:
		pneumatics = [1.0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	else:
		pneumatics = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	msg = Floats()
	msg = pneumatics
	pub.publish(msg)
	
	rate.sleep()

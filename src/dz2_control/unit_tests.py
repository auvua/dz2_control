import unittest
import numpy as np
import math
import tf
from project_utils import project_force_onto_thruster, project_torque_onto_thruster
from dz2_control import calculate_forces

EPSILON = 1.0e-6

class ProjectUtilsTest(unittest.TestCase):
    def test_centered_thruster(self):
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, 0]), (1,0,0)), 1.0)
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, 0]), (0,1,0)), 0.0)
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, 0]), (0,0,1)), 0.0)
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, 0]), (0.5,0,0)), 0.5)
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, 0]), (-1.0,0,0)), -1.0)
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, 0]), (1,1,1)), 1.0)

    def test_rotated_thruster(self):
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, math.pi/4, 0, 0]), (1,0,0)), 1.0)
        self.assertLessEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, math.pi/4, 0]), (1,0,0)), 1/math.sqrt(2.0) + EPSILON)
        self.assertGreaterEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, math.pi/4, 0]), (1,0,0)), 1/math.sqrt(2.0) - EPSILON)
        self.assertLessEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, math.pi/4]), (1,0,0)), 1/math.sqrt(2.0) + EPSILON)
        self.assertGreaterEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 0, math.pi/4]), (1,0,0)), 1/math.sqrt(2.0) - EPSILON)
        self.assertLessEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, math.pi/2, 0]), (1,0,0)), EPSILON)
        self.assertLessEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 3*math.pi/4, 0]), (1,0,0)), -1/math.sqrt(2.0) + EPSILON)
        self.assertGreaterEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 3*math.pi/4, 0]), (1,0,0)), -1/math.sqrt(2.0) - EPSILON)
        self.assertEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, math.pi, 0]), (1,0,0)), -1)
        self.assertLessEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 5*math.pi/4, 0]), (1,0,0)), -1/math.sqrt(2.0) + EPSILON)
        self.assertGreaterEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 5*math.pi/4, 0]), (1,0,0)), -1/math.sqrt(2.0) - EPSILON)
        self.assertLessEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 3*math.pi/2, 0]), (1,0,0)), EPSILON)
        self.assertLessEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 7*math.pi/4, 0]), (1,0,0)), 1/math.sqrt(2.0) + EPSILON)
        self.assertGreaterEqual(project_force_onto_thruster(np.array([0, 0, 0, 0, 7*math.pi/4, 0]), (1,0,0)), 1/math.sqrt(2.0) - EPSILON)

class DZ2ControlsTest(unittest.TestCase):
    def test_translation(self):
        tests = [(1.0, 0, 0), (2.0, 0, 0), (-1.0, 0, 0), (-2.0, 0, 0),
                 (0, 1.0, 0), (0, 2.0, 0), (0, -1.0, 0), (0, -2.0, 0),
                 (0, 0, 1.0), (0, 0, 2.0), (0, 0, -1.0), (0, 0, -2.0), (-5, 0, 0)]
        print calculate_forces((0, 0, 0), tf.transformations.quaternion_from_euler(math.pi/2, 0, 0))
        print calculate_forces((0, 0, 0), tf.transformations.quaternion_from_euler(5, 0, 0))
        for i in range(len(tests)):
            self.assertItemsEqual(calculate_forces((0, 0, 0),
                tf.transformations.quaternion_from_euler(tests[i][0], tests[i][1], tests[i][2]))[1], np.array(tests[i]))

if __name__=='__main__':
    unittest.main()

import numpy as np
import math
import time
import rospy
import roslib
import tf
import tf2_ros
import tf2_geometry_msgs

from rospy_tutorials.msg import Floats
from geometry_msgs.msg import PoseStamped, Vector3Stamped, TransformStamped, Wrench
from force_utils import *
from urdf_parser_py.urdf import URDF
from dynamic_reconfigure.server import Server
from dz2_control.cfg import ParamsConfig
from dz2_control.pid import PID

robot = {}
wrench = Wrench()
params = {
    'pid_roll': PID(1.0, 0.0, 0.0, -50.0, 50.0),
    'pid_pitch': PID(1.0, 0.0, 0.0, -50.0, 50.0),
    'pid_yaw': PID(1.0, 0.0, 0.0, -50.0, 50.0),
    'pid_depth': PID(1.0, 0.0, 0.0, -50.0, 50.0),
    'maxout': 1.0
}

def reconfig_callback(config, level):
    global params

    controllers = ['pid_roll', 'pid_pitch', 'pid_yaw', 'pid_depth']

    for c in controllers:
        if params[c].enable is False and config[c + '_en'] is True:
            params[c].reset()
            params[c].enable = True
        elif config[c + '_en'] is False:
            params[c].enable = False

        params[c].kp = config[c + '_kp']
        params[c].ki = config[c + '_ki']
        params[c].kd = config[c + '_kd']

    params['maxout'] = config['maxout']

    return config

def wrench_callback(msg):
    """
    Receives and stores the desired open loop vehicle force screw (geometry_msgs.Wrench)

    :param msg: ros Subscriber message, msg.data is geometry_msgs.Wrench
    :return:
    """
    global wrench
    wrench = msg

def main():
    global robot
    global wrench
    global params

    thrusters = {}

    rospy.init_node('dz2_control')
    robot = URDF.from_parameter_server('robot_description')
    cfgsrv = Server(ParamsConfig, reconfig_callback)
    thruster_pub = rospy.Publisher('/thrusters', Floats, queue_size=10)
    rospy.Subscriber('/cmd_force', Wrench, wrench_callback)
    listener = tf.TransformListener()
    rate = rospy.Rate(50)
    lastmsg = rospy.get_time()

    while not rospy.is_shutdown():
        try:
            lastmsg = listener.getLatestCommonTime('/goal', '/base_link').to_sec()
            if rospy.get_time() - lastmsg > 1.0:
                trans = (0, 0, 0)
                rot = tf.transformations.quaternion_from_euler(0, 0, 0)
            else:
                (trans, rot) = listener.lookupTransform('/goal', '/base_link', rospy.Time(0))
        except (tf.Exception, tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "Exception!"
            rate.sleep()
            continue

        # Get the vector components and magnitudes
        trans_mag = np.linalg.norm(trans)
        (roll_angle, pitch_angle, yaw_angle) = tf.transformations.euler_from_quaternion(rot)

        # Update PID loops with magnitudes
        curr_time = time.time()
        trans_mag_new = params['pid_depth'].update(curr_time, trans_mag)
        roll_out = params['pid_roll'].update(curr_time, roll_angle)
        pitch_out = params['pid_pitch'].update(curr_time, pitch_angle)
        yaw_out = params['pid_yaw'].update(curr_time, yaw_angle)

        # Rep as force screw
        if trans_mag > 0:
            force_vector = np.multiply(trans, trans_mag_new/trans_mag)
        else:
	        force_vector = (0, 0, 0)
        torque_vector = np.array([roll_out, pitch_out, yaw_out])

        # Add open loop force screw
        force_vector = (force_vector[0] + wrench.force.x,
                        force_vector[1] + wrench.force.y,
                        force_vector[2] + wrench.force.z)
        torque_vector = (torque_vector[0] + wrench.torque.x,
                         torque_vector[1] + wrench.torque.y,
                         torque_vector[2] + wrench.torque.z)

        #print "Force: " + str(force_vector)
        #print "Torque: " + str(torque_vector)

        for joint in robot.joints:
            if joint.child[:8] == 'thruster':
                [x, y, z] = joint.origin.xyz
                [r, p, yaw] = joint.origin.rpy
                thruster = np.array([x, y, z, r, p, yaw])

                thrusters[joint.name] = float(project_force_onto_thruster(thruster, force_vector)) \
                                      + float(project_torque_onto_thruster(thruster, torque_vector))
        thruster_msg = Floats()

        thruster_output = [
            bound(thrusters['body_2_ttfr'], params['maxout']),
            bound(thrusters['body_2_ttfl'], params['maxout']),
            bound(thrusters['body_2_tvfl'], params['maxout']),
            bound(thrusters['body_2_tvrl'], params['maxout']),
            bound(thrusters['body_2_ttrl'], params['maxout']),
            bound(thrusters['body_2_ttrr'], params['maxout']),
            bound(thrusters['body_2_tvrr'], params['maxout']),
            bound(thrusters['body_2_tvfr'], params['maxout']), 0, 0]
        #print str(thruster_output)

        thruster_msg = thruster_output

        thruster_pub.publish(thruster_msg)

        rate.sleep()

if __name__ == '__main__':
    main()

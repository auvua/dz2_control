import numpy as np
import math
import tf
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped, Vector3Stamped, TransformStamped

def bound(x, max):
    if x is None:
        return 0
    if(x < -max): return -max
    if(x > max): return max
    return x

def project_torque_onto_thruster(thruster, torque):
    """
    Projects a desired vehicle-centric torque onto a thruster's principle axis

    :param thruster: location/orientation as a list [x,y,z,r,p,yaw]
    :param torque: torque vector as a numpy array [x,y,z]
    :return: force (scalar) along thruster axis
    """
    radius = np.array(thruster[:3])
    force_at_thruster = np.cross(torque, radius) / (np.linalg.norm(radius)**2)

    return project_force_onto_thruster(thruster, force_at_thruster)


def project_force_onto_thruster(thruster, force):
    """
    Projects a desired vehicle-centric force onto a thruster's principle axis

    :param thruster: location/orientation as a list [x,y,z,r,p,yaw]
    :param force: force vector as a numpy array [x,y,z]
    :return: force (scalar) along thruster axis
    """
    [roll, pitch, yaw] = thruster[3:]
    q_raw = tf.transformations.quaternion_from_euler(roll, pitch, yaw)

    t = TransformStamped()
    t.transform.rotation.x = q_raw[0]
    t.transform.rotation.y = q_raw[1]
    t.transform.rotation.z = q_raw[2]
    t.transform.rotation.w = q_raw[3]

    v = Vector3Stamped()
    v.vector.x = 1
    v.vector.y = 0
    v.vector.z = 0

    vt = tf2_geometry_msgs.do_transform_vector3(v, t)
    thruster_vector = np.array([vt.vector.x, vt.vector.y, vt.vector.z])
    projected_force = np.inner(force, thruster_vector) * thruster_vector

    return np.inner(projected_force, thruster_vector)

def get_rotation_comps(rot):
    """
    Returns a rotation vector and angular displacement from a quaternion.
    """
    rot_angle = 2 * np.arccos(rot[3])
    rot_angle = (rot_angle + math.pi) % (2*math.pi) - math.pi

    rot_vector = np.array([1.0, 0.0, 0.0])
    s_sqr = 1.0 - rot[3]**2.0

    if(s_sqr < 0.000001):
        rot_vector = np.array([1.0, 0.0, 0.0])
    else:
        s = np.sqrt(s_sqr)
        rot_vector = np.array([rot[0]/s, rot[1]/s, rot[2]/s])

    return (rot_vector, rot_angle)

def calculate_forces(trans, rot, scaling_trans=1.0, scaling_rot=1.0):
    """
    Calculates a force screw to apply to bring trans and rot to 0.
    """
    target_force = np.multiply(trans, scaling_trans)
    target_torque = (0, 0, 0)

    (rot_vector, rot_angle) = get_rotation_comps(rot)

    target_torque = rot_vector * rot_angle * scaling_rot

    return (target_force, target_torque)


if __name__=='__main__':
    assert project_force_onto_thruster(np.array([0, 0, 0, 0, 0, 0]), (1,0,0)) == 1.0
    #assert project_force_onto_thruster(np.array([1, -1, 0, 3.14159/4.0, 0, 0]), (0,0,1))

    #print project_torque_onto_thruster(np.array([0.7071, -0.7071, 0, 0, 0, 3.14159/4.0]), (0,0,-1))

import time

class PID:
    def __init__(self, kp=0.0, ki=0.0, kd=0.0, lower=-1.0, upper=1.0):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.lower = lower
        self.upper = upper
        self.err = 0.0
        self.err_prev = 0.0
        self.err_int = 0.0
        self.err_int_max = self.upper
        self.err_der = 0.0
        self.time_prev = time.time()
        self.value_prev = 0.0
        self.output = 0.0
        self.target = 0.0
        self.enable = False

    def _bound(self, value):
        if value > self.upper:
            return self.upper
        elif value < self.lower:
            return self.lower
        else:
            return value

    def set_target(self, value):
        self.target = value

    def update(self, time_curr, value):
        time_delta = time_curr - self.time_prev
        self.err = self.target - value
        if time_delta > 0:
            self.err_der = (self.err - self.err_prev) / time_delta
        self.err_int = self._bound(self.err_int + (self.err * time_delta))

        self.output = self.kp*self.err + self.ki*self.err_int + self.kd*self.err_der

        self.value_prev = value
        self.err_prev = self.err
        self.time_prev = time_curr

        if self.enable:
            return self._bound(self.output)
        else:
            return 0.0

    def reset(self):
        self.output = 0.0
        self.err = 0.0
        self.err_int = 0.0
        self.err_prev = 0.0
        self.value_prev = 0.0
